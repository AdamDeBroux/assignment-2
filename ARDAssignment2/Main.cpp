#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2) {
	return num1 + num2;
}
float Subtract(float num1, float num2) {
	return num1 - num2;
}
float Multiply(float num1, float num2) {
	return num1 * num2;
}
bool Divide(float num1, float num2, float& answer) {
	if (num2 == 0) return false;
	answer = num1 / num2;
	return true;

}

int main() {
	float x;
	float y;
	char s;
	char a;
	float ans;
	cout << "Enter a positive number.\n";
	cin >> x;
	cout << "Enter another positive number.\n";
	cin >> y;
	cout << "Enter an operator.\n";
	cin >> s;
	switch (s) {
	case '+':
		ans = Add(x, y);
		cout << "Your answer is: " << ans << "\n";
		break;
	case '-':
		ans = Subtract(x, y);
		cout << "Your answer is: " << ans << "\n";
		break;
	case '*':
		ans = Multiply(x, y);
		cout << "Your answer is: " << ans << "\n";
		break;
	case '/':
		if (!Divide(x, y, ans)) {
			cout << "Hey brainlet, you can't divide by 0";
		}
		else {
			cout << "Your answer is: " << ans << "\n";
		}
		break;
	default:
		cout << "That's not an operator. Are you dumb? Do you think it's funny to waste my time like this?\n";
	}
	cout << "Would you like to perform another calculation?\n";
	cin >> a;
	switch (a) {
	case 'Y':
		main();
		break;
	case 'y':
		main();
		break;
	case 'N':
		break;
	case 'n':
		break;
	default:
		cout << "Are you serious? You can't answer a simple yes or no question? What is wrong with you? I'm done.\n";
	}

	_getch();
	return 0;
}